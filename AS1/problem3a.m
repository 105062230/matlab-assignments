close all;
clear all;
clc;
img = imread('data\cat3_LR.png');
img = double(img);
[m n r] = size(img);

img_r3 = zeros(m+2,n+2);
img_g3 = zeros(m+2,n+2);
img_b3 = zeros(m+2,n+2);

img_r3(2:m+1,2:n+1) = img(:,:,1);
img_g3(2:m+1,2:n+1) = img(:,:,2);
img_b3(2:m+1,2:n+1) = img(:,:,3);

new_r = zeros(m,n);
new_g = zeros(m,n);
new_b = zeros(m,n);
new = zeros(m,n,r);

G3 = fspecial('gaussian',[3 3],1);

for i=2:m+1
    for j=2:n+1
        M3r = img_r3(i-1:i+1,j-1:j+1);
        M3g = img_g3(i-1:i+1,j-1:j+1);
        M3b = img_b3(i-1:i+1,j-1:j+1);
        new_r(i-1,j-1)= sum(sum(M3r .* G3));
        new_g(i-1,j-1)= sum(sum(M3g .* G3));
        new_b(i-1,j-1)= sum(sum(M3b .* G3));
    end
end

new(:,:,1) = new_r;
new(:,:,2) = new_g;
new(:,:,3) = new_b;
new = uint8(new);
imwrite(new, '3a-filter3.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR

img_r5 = zeros(m+4,n+4);
img_g5 = zeros(m+4,n+4);
img_b5 = zeros(m+4,n+4);

img_r5(3:m+2,3:n+2) = img(:,:,1);
img_g5(3:m+2,3:n+2) = img(:,:,2);
img_b5(3:m+2,3:n+2) = img(:,:,3);

G5 = fspecial('gaussian',[5 5],1);

for i=3:m+2
    for j=3:n+2
        M5r = img_r5(i-2:i+2,j-2:j+2);
        M5g = img_g5(i-2:i+2,j-2:j+2);
        M5b = img_b5(i-2:i+2,j-2:j+2);

        new_r(i-2,j-2)= sum(sum(M5r .* G5));
        new_g(i-2,j-2)= sum(sum(M5g .* G5));
        new_b(i-2,j-2)= sum(sum(M5b .* G5));
    end
end

new(:,:,1) = new_r;
new(:,:,2) = new_g;
new(:,:,3) = new_b;
new = uint8(new);
imwrite(new, '3a-filter5.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR

img_r7 = zeros(m+6,n+6);
img_g7 = zeros(m+6,n+6);
img_b7 = zeros(m+6,n+6);

img_r7(4:m+3,4:n+3) = img(:,:,1);
img_g7(4:m+3,4:n+3) = img(:,:,2);
img_b7(4:m+3,4:n+3) = img(:,:,3);

G7 = fspecial('gaussian',[7 7],1);

for i=4:m+3
    for j=4:n+3
        M7r = img_r7(i-3:i+3,j-3:j+3);
        M7g = img_g7(i-3:i+3,j-3:j+3);
        M7b = img_b7(i-3:i+3,j-3:j+3);
        new_r(i-3,j-3)= sum(sum(M7r .* G7));
        new_g(i-3,j-3)= sum(sum(M7g .* G7));
        new_b(i-3,j-3)= sum(sum(M7b .* G7));
    end
end

new(:,:,1) = new_r;
new(:,:,2) = new_g;
new(:,:,3) = new_b;
new = uint8(new);
imwrite(new, '3a-filter7.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR