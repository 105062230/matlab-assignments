close all;
clear all;
clc;
img=imread('data\cat2_gray.png');
[m,n]=size(img);

new=zeros(m,n);
tmp=zeros(m+2,n+2);
tmp(2:m+1,2:n+1)=double(img);

for i=2:m+1
    for j=2:n+1
        if tmp(i,j)<128
            new(i-1,j-1) = 0;
            e=tmp(i,j);
        else
            new(i-1,j-1) = 255;
            e=tmp(i,j)-255;
        end
        tmp(i,j+1)=tmp(i,j+1)+7/16*e;
        tmp(i+1,j-1)=tmp(i+1,j-1)+3/16*e;
        tmp(i+1,j)=tmp(i+1,j)+5/16*e;
        tmp(i+1,j+1)=tmp(i+1,j+1)+1/16*e;
    end
end

new = uint8(new);
imwrite(new, '2c.png');