close all;
clear all;
clc;
img = imread('data\cat1.png');
imshow(img);
img = double(img);
[m,n,k] = size(img);

img_r(1:m,1:n) = img(:,:,1);
img_g(1:m,1:n) = img(:,:,2);
img_b(1:m,1:n) = img(:,:,3);

F_r = zeros(m,n,3);
F_g = zeros(m,n,3);
F_b = zeros(m,n,3);

f_r = zeros(m,n,3);
f_g = zeros(m,n,3);
f_b = zeros(m,n,3);

cmpres2 = zeros(8,8);
cmpres2(1,:) = [1,1,0,0,0,0,0,0];
cmpres2(2,:) = [1,1,0,0,0,0,0,0];
cmpres4 = zeros(8,8);
cmpres4(1,:) = [1,1,1,1,0,0,0,0];
cmpres4(2,:) = [1,1,1,1,0,0,0,0];
cmpres4(3,:) = [1,1,1,1,0,0,0,0];
cmpres4(4,:) = [1,1,1,1,0,0,0,0];

Y = zeros(m,n);
I = Y;
Q = Y;

for i=1:m
    for j=1:n
            Y(i,j) = 0.299*img_r(i,j)+0.587*img_g(i,j)+0.114*img_b(i,j);
            I(i,j) = 0.596*img_r(i,j)-0.275*img_g(i,j)-0.321*img_b(i,j);
            Q(i,j) = 0.212*img_r(i,j)-0.523*img_g(i,j)+0.311*img_b(i,j);
    end
end

img_r = Y;
img_g = I;
img_b = Q;

for row=1:m
    for col=1:n
        u = mod(row, 8);
        if u==0
            u = 7;
        else
            u = u - 1;
        end
        v = mod(col, 8);
        if v==0
            v = 7;
        else
            v = v - 1;
        end
        blk_r = (ceil(row/8)-1) * 8 + 1;
        blk_c = (ceil(col/8)-1) * 8 + 1;
        for r=0:7
           for s=0:7
               t_r = 1/4*img_r(blk_r+r,blk_c+s)*(cos((2*r+1)*u*pi/16))*(cos((2*s+1)*v*pi/16));
               t_g = 1/4*img_g(blk_r+r,blk_c+s)*(cos((2*r+1)*u*pi/16))*(cos((2*s+1)*v*pi/16));
               t_b = 1/4*img_b(blk_r+r,blk_c+s)*(cos((2*r+1)*u*pi/16))*(cos((2*s+1)*v*pi/16));
               if u==0
                   t_r = t_r * sqrt(2) / 2;
                   t_g = t_g * sqrt(2) / 2;
                   t_b = t_b * sqrt(2) / 2;
               end
               if v==0
                   t_r = t_r * sqrt(2) / 2;
                   t_g = t_g * sqrt(2) / 2;
                   t_b = t_b * sqrt(2) / 2;
               end
               F_r(row,col,1) = F_r(row,col,1) + t_r;
               F_g(row,col,1) = F_g(row,col,1) + t_g;
               F_b(row,col,1) = F_b(row,col,1) + t_b;
           end
        end
        tmp_r = F_r(blk_r:blk_r + 7,blk_c:blk_c + 7,1);
        tmp_g = F_g(blk_r:blk_r + 7,blk_c:blk_c + 7,1);
        tmp_b = F_b(blk_r:blk_r + 7,blk_c:blk_c + 7,1);
        F_r(blk_r:blk_r + 7,blk_c:blk_c + 7,2) = tmp_r .* cmpres4;
        F_g(blk_r:blk_r + 7,blk_c:blk_c + 7,2) = tmp_g .* cmpres4;
        F_b(blk_r:blk_r + 7,blk_c:blk_c + 7,2) = tmp_b .* cmpres4;
        F_r(blk_r:blk_r + 7,blk_c:blk_c + 7,3) = tmp_r .* cmpres2;
        F_g(blk_r:blk_r + 7,blk_c:blk_c + 7,3) = tmp_g .* cmpres2;
        F_b(blk_r:blk_r + 7,blk_c:blk_c + 7,3) = tmp_b .* cmpres2;
    end
end


f4 = F_r(:,:,2);

for row=0:(ceil(m/8)-1)
    for col=0:(ceil(n/8)-1)
        cmp_r = row*8+1;
        cmp_c = col*8+1;
        tmp_r = F_r(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,1);
        tmp_g = F_g(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,1);
        tmp_b = F_b(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,1);
        F_r(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,2) = tmp_r .* cmpres4;
        F_g(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,2) = tmp_g .* cmpres4;
        F_b(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,2) = tmp_b .* cmpres4;
        F_r(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,3) = tmp_r .* cmpres2;
        F_g(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,3) = tmp_g .* cmpres2;
        F_b(cmp_r:cmp_r + 7,cmp_c:cmp_c + 7,3) = tmp_b .* cmpres2;
    end
end

for row=1:m
    for col=1:n
        r = mod(row, 8);
        if r==0
            r = 7;
        else
            r = r - 1;
        end
        s = mod(col, 8);
        if s==0
            s = 7;
        else
            s = s - 1;
        end
        blk_r = (ceil(row/8)-1) * 8 + 1;
        blk_c = (ceil(col/8)-1) * 8 + 1;
        for u=0:7
            for v=0:7
                for i=1:3
                   t_r = 1/4*F_r(blk_r+u,blk_c+v,i)*(cos((2*r+1)*u*pi/16))*(cos((2*s+1)*v*pi/16));
                   t_g = 1/4*F_g(blk_r+u,blk_c+v,i)*(cos((2*r+1)*u*pi/16))*(cos((2*s+1)*v*pi/16));
                   t_b = 1/4*F_b(blk_r+u,blk_c+v,i)*(cos((2*r+1)*u*pi/16))*(cos((2*s+1)*v*pi/16)); 
                   if u==0
                       t_r = t_r * sqrt(2) / 2;
                       t_g = t_g * sqrt(2) / 2;
                       t_b = t_b * sqrt(2) / 2;
                   end
                   if v==0
                       t_r = t_r * sqrt(2) / 2;
                       t_g = t_g * sqrt(2) / 2;
                       t_b = t_b * sqrt(2) / 2;
                   end
                   f_r(row,col,i) = f_r(row,col,i) + t_r;
                   f_g(row,col,i) = f_g(row,col,i) + t_g;
                   f_b(row,col,i) = f_b(row,col,i) + t_b;
                end
            end
        end
    end
end

y_ = f_r;
i_ = f_g;
q_ = f_b;

for i=1:m
    for j=1:n
        for c=1:3
            f_r(i,j,c) = y_(i,j,c)+0.956*i_(i,j,c)+0.620*q_(i,j,c);
            f_g(i,j,c) = y_(i,j,c)-0.272*i_(i,j,c)-0.647*q_(i,j,c);
            f_b(i,j,c) = y_(i,j,c)-1.108*i_(i,j,c)+1.705*q_(i,j,c);
        end
    end
end

new = zeros(m,n,k);

new(:,:,1) = f_r(:,:,1);
new(:,:,2) = f_g(:,:,1);
new(:,:,3) = f_b(:,:,1);

new = uint8(new);
imshow(new);
imwrite(new, '1b-n8.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR

new(:,:,1) = f_r(:,:,2);
new(:,:,2) = f_g(:,:,2);
new(:,:,3) = f_b(:,:,2);

new = uint8(new);
imshow(new);
imwrite(new, '1b-n4.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR

new(:,:,1) = f_r(:,:,3);
new(:,:,2) = f_g(:,:,3);
new(:,:,3) = f_b(:,:,3);

new = uint8(new);
imshow(new);
imwrite(new, '1b-n2.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR