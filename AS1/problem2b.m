close all;
clear all;
clc;
img=imread('data\cat2_gray.png');
[m n]=size(img);

new=zeros(m,n);
img=double(img);

mean = mean2(img);

for i=1:m
    for j=1:n
        if img(i,j)>mean
            new(i,j) = 255;
        else
            new(i,j) = 0;
        end
    end
end

new = uint8(new);
imwrite(new, '2b.png');