close all;
clear all;
clc;
img=imread('data\cat2_gray.png');
imshow(img);
[m n]=size(img);

new=zeros(m,n);
img=double(img);


for i=1:m
    for j=1:n
        r = round(rand(1,1)*255);
        if img(i,j)>r
            new(i,j) = 255;
        else
            new(i,j) = 0;
        end
    end
end

new = uint8(new);
imwrite(new, '2a.png');
