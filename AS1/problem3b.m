close all;
clear all;
clc;
img = imread('data\cat3_LR.png');
img = double(img);
[m n r] = size(img);

new_r = zeros(m,n);
new_g = zeros(m,n);
new_b = zeros(m,n);
new = zeros(m,n,r);

img_r5 = zeros(m+4,n+4);
img_g5 = zeros(m+4,n+4);
img_b5 = zeros(m+4,n+4);

img_r5(3:m+2,3:n+2) = img(:,:,1);
img_g5(3:m+2,3:n+2) = img(:,:,2);
img_b5(3:m+2,3:n+2) = img(:,:,3);

S1 = fspecial('gaussian',[5 5],1);
G5 = S1;

for i=3:m+2
    for j=3:n+2
        M5r = img_r5(i-2:i+2,j-2:j+2);
        M5g = img_g5(i-2:i+2,j-2:j+2);
        M5b = img_b5(i-2:i+2,j-2:j+2);

        new_r(i-2,j-2)= sum(sum(M5r .* G5));
        new_g(i-2,j-2)= sum(sum(M5g .* G5));
        new_b(i-2,j-2)= sum(sum(M5b .* G5));
    end
end

new(:,:,1) = new_r;
new(:,:,2) = new_g;
new(:,:,3) = new_b;
new = uint8(new);
imwrite(new, '3b-sigma1.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR


S5 = fspecial('gaussian',[5 5],5);
G5 = S5;

for i=3:m+2
    for j=3:n+2
        M5r = img_r5(i-2:i+2,j-2:j+2);
        M5g = img_g5(i-2:i+2,j-2:j+2);
        M5b = img_b5(i-2:i+2,j-2:j+2);

        new_r(i-2,j-2)= sum(sum(M5r .* G5));
        new_g(i-2,j-2)= sum(sum(M5g .* G5));
        new_b(i-2,j-2)= sum(sum(M5b .* G5));
    end
end

new(:,:,1) = new_r;
new(:,:,2) = new_g;
new(:,:,3) = new_b;
new = uint8(new);
imwrite(new, '3b-sigma5.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR

S10 = fspecial('gaussian',[5 5],10);
G5 = S10;

for i=3:m+2
    for j=3:n+2
        M5r = img_r5(i-2:i+2,j-2:j+2);
        M5g = img_g5(i-2:i+2,j-2:j+2);
        M5b = img_b5(i-2:i+2,j-2:j+2);

        new_r(i-2,j-2)= sum(sum(M5r .* G5));
        new_g(i-2,j-2)= sum(sum(M5g .* G5));
        new_b(i-2,j-2)= sum(sum(M5b .* G5));
    end
end

new(:,:,1) = new_r;
new(:,:,2) = new_g;
new(:,:,3) = new_b;
new = uint8(new);
imwrite(new, '3b-sigma10.png');

MSE = sum(sum(sum((double(img)-double(new)).^2)))/(m*n*3);
PSNR = 10*log10(255^2/MSE);
PSNR