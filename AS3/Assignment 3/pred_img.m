function pred = pred_img(ref, motion_vect, s)

[row, col, ch] = size(ref);
for i = 1:s:row-s+1
    for j = 1:s:col-s+1
        refBlkVer = motion_vect(i,j,1);
        refBlkHor = motion_vect(i,j,2);
        img_pred(i:i+s-1,j:j+s-1,1) = ref(refBlkVer:refBlkVer+s-1, refBlkHor:refBlkHor+s-1,1);
        img_pred(i:i+s-1,j:j+s-1,2) = ref(refBlkVer:refBlkVer+s-1, refBlkHor:refBlkHor+s-1,2);
        img_pred(i:i+s-1,j:j+s-1,3) = ref(refBlkVer:refBlkVer+s-1, refBlkHor:refBlkHor+s-1,3);
    end
end
pred = img_pred;
