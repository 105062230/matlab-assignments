function res = res_img(pred, tar)
[row ,col, ch] = size(tar); 
res = zeros(row ,col);

for i = 1:row
    for j = 1:col
        res(i,j) = abs(pred(i,j,1)-tar(i,j,1)) + abs(pred(i,j,2)-tar(i,j,2)) + abs(pred(i,j,3)-tar(i,j,3)) ;
    end
end
end

