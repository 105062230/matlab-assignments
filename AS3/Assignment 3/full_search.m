function [motion_vect, sad_sum] = full_search(tar, ref, s, p)

[row, col, ch] = size(ref);
vectors = zeros(row,col,2);
sads = ones(2*p + 1, 2*p +1) * 65537;
sad_sum = 0;
for i = 1 : s : row-s+1
    for j = 1 : s : col-s+1
        curblk = tar(i:i+s-1,j:j+s-1,:);
        sads = ones(2*p + 1, 2*p +1) * 65537;
        for m = -p : p        
            for n = -p : p
                refBlkVer = i + m;  
                refBlkHor = j + n;  
                if ( refBlkVer < 1 || refBlkVer+s-1 > row || refBlkHor < 1 || refBlkHor+s-1 > col)
                    continue;
                end
                sads(m+p+1,n+p+1) = SAD(curblk,ref(refBlkVer:refBlkVer+s-1, refBlkHor:refBlkHor+s-1,:), s);
            end
        end
        [c_r, c_c] = size(sads);
        min = 65537;
        for ci = 1:c_r
            for cj = 1:c_c
                if (sads(ci,cj) < min)
                    min = sads(ci,cj);
                    dx = cj; dy = ci;
                end
            end
        end
        sad_sum = sad_sum + min;
        vectors(i,j,1) = i + dy - p - 1;
        vectors(i,j,2) = j + dx - p - 1;  
        sads = ones(2*p + 1, 2*p +1) * 65537;
    end
end
motion_vect = vectors;
end
