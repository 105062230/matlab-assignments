%%%  two search ranges (p=8 and p=16) and two macroblock sizes (8x8
%%%  and 16x16) by using the two search methods
clear;
clc;
ref = double(imread('data\frame437.jpg'));
tar = double(imread('data\frame439.jpg'));
[row, col, ch] = size(tar);

s = [8, 16];
p = [8, 16];
psnr_val = zeros(4:2);
sad_val = zeros(4:2);
t1 = zeros(2,1);
t2 = zeros(2,1);

pos = zeros(row,col,2);

for i = 1:row
    for j = 1:col
        pos(i,j,1) = i;
        pos(i,j,2) = j;
    end
end

for i = 1:2
    for j = 1:2
        if i==1
            timer1=clock;
        end
        [motion_vect3, sad_val3] = three_steps(tar,ref,s(i),p(j));
        img_pred = pred_img(ref, motion_vect3, s(i));
        if i==1 
            t1(j,1) = etime(clock, timer1);
        end
        residual = res_img(img_pred, tar);
        pred3 = uint8(img_pred);
        res3 = uint8(residual);
        psnr_val3 = cal_PSNR(uint8(tar), pred3);
        
        if i==1
            timer2=clock;
        end
        [motion_vect_f, sad_val_f] = full_search(tar,ref,s(i),p(j));
        img_pred = pred_img(ref, motion_vect_f, s(i));
        if i==1 
            t2(j,1) = etime(clock, timer2);
        end
        residual = res_img(img_pred, tar);
        pred_f = uint8(img_pred);
        res_f = uint8(residual);
        psnr_val_f = cal_PSNR(uint8(tar), pred_f);
        
        for m = 1:row
            for n = 1:col
                if (motion_vect3(m,n,1)==0 && motion_vect3(m,n,2)==0)
                    continue;
                end
                motion_vect3(m,n,1) = motion_vect3(m,n,1) - pos(m,n,1);
                motion_vect3(m,n,2) = motion_vect3(m,n,2) - pos(m,n,2);
            end
        end
        
        for m = 1:row
            for n = 1:col
                if (motion_vect_f(m,n,1)==0 && motion_vect_f(m,n,2)==0)
                    continue;
                end
                motion_vect_f(m,n,1) = motion_vect_f(m,n,1) - pos(m,n,1);
                motion_vect_f(m,n,2) = motion_vect_f(m,n,2) - pos(m,n,2);
            end
        end
        
        if(i==1&&j==1)
            imwrite(pred3,'predict\three_step_s8_p8.jpg');
            imwrite(res3,'residual\three_step_s8_p8.jpg');
            imwrite(pred_f,'predict\full_s8_p8.jpg');
            imwrite(res_f,'residual\full_s8_p8.jpg');
            sad_val(1,1) = sad_val3;
            sad_val(1,2) = sad_val_f;
            psnr_val(1,1) = psnr_val3;
            psnr_val(1,2) = psnr_val_f;
            
            figure(1);
            x = (1:col);
            y = (1:row);
            u = motion_vect3(y,x,1);
            v = motion_vect3(y,x,2);
            quiver(x,y,u,v);axis image
            hax = gca;
            image(hax.XLim,hax.YLim,uint8(tar),'AlphaData',0.3);
            hold on;
            quiver(x,y,u,v);
            title('three step s8 p8');
            
            figure(2);
            u = motion_vect_f(y,x,1);
            v = motion_vect_f(y,x,2);
            quiver(x,y,u,v);axis image
            hax = gca;
            image(hax.XLim,hax.YLim,uint8(tar),'AlphaData',0.3);
            hold on;
            quiver(x,y,u,v);
            title('full s8 p8');
        elseif(i==1&&j==2)
            imwrite(pred3,'predict\three_step_s8_p16.jpg');
            imwrite(res3,'residual\three_step_s8_p16.jpg');
            imwrite(pred_f,'predict\full_s8_p16.jpg');
            imwrite(res_f,'residual\full_s8_p16.jpg');
            sad_val(2,1) = sad_val3;
            sad_val(2,2) = sad_val_f;
            psnr_val(2,1) = psnr_val3;
            psnr_val(2,2) = psnr_val_f;
            
            figure(3);
            x = (1:col);
            y = (1:row);
            u = motion_vect3(y,x,1);
            v = motion_vect3(y,x,2);
            quiver(x,y,u,v);axis image
            hax = gca;
            image(hax.XLim,hax.YLim,uint8(tar),'AlphaData',0.3);
            hold on;
            quiver(x,y,u,v);
            title('three step s8 p16');
            
            figure(4);
            u = motion_vect_f(y,x,1);
            v = motion_vect_f(y,x,2);
            quiver(x,y,u,v);axis image
            hax = gca;
            image(hax.XLim,hax.YLim,uint8(tar),'AlphaData',0.3);
            hold on;
            quiver(x,y,u,v);
            title('full s8 p16');
            
        elseif(i==2&&j==1)
            imwrite(pred3,'predict\three_step_s16_p8.jpg');
            imwrite(res3,'residual\three_step_s16_p8.jpg');
            imwrite(pred_f,'predict\full_s16_p8.jpg');
            imwrite(res_f,'residual\full_s16_p8.jpg');
            sad_val(3,1) = sad_val3;
            sad_val(3,2) = sad_val_f;
            psnr_val(3,1) = psnr_val3;
            psnr_val(3,2) = psnr_val_f;
            
            figure(5);
            x = (1:col);
            y = (1:row);
            u = motion_vect3(y,x,1);
            v = motion_vect3(y,x,2);
            quiver(x,y,u,v);axis image
            hax = gca;
            image(hax.XLim,hax.YLim,uint8(tar),'AlphaData',0.3);
            hold on;
            quiver(x,y,u,v);
            title('three step s16 p8');
            
            figure(6);
            u = motion_vect_f(y,x,1);
            v = motion_vect_f(y,x,2);
            quiver(x,y,u,v);axis image
            hax = gca;
            image(hax.XLim,hax.YLim,uint8(tar),'AlphaData',0.3);
            hold on;
            quiver(x,y,u,v);
            title('full s16 p8');
        else
            imwrite(pred3,'predict\three_step_s16_p16.jpg');
            imwrite(res3,'residual\three_step_s16_p16.jpg');
            imwrite(pred_f,'predict\full_s16_p16.jpg');
            imwrite(res_f,'residual\full_s16_p16.jpg');
            sad_val(4,1) = sad_val3;
            sad_val(4,2) = sad_val_f;
            psnr_val(4,1) = psnr_val3;
            psnr_val(4,2) = psnr_val_f;
            
            figure(7);
            x = (1:col);
            y = (1:row);
            u = motion_vect3(y,x,1);
            v = motion_vect3(y,x,2);
            quiver(x,y,u,v);axis image
            hax = gca;
            image(hax.XLim,hax.YLim,uint8(tar),'AlphaData',0.3);
            hold on;
            quiver(x,y,u,v);
            title('three step s16 p16');
            
            figure(8);
            u = motion_vect_f(y,x,1);
            v = motion_vect_f(y,x,2);
            quiver(x,y,u,v);
            quiver(x,y,u,v);axis image
            hax = gca;
            image(hax.XLim,hax.YLim,uint8(tar),'AlphaData',0.3);
            hold on;
            quiver(x,y,u,v);
            title('full s16 p16');
        end
    end
end

figure(9)
subplot(2,1,1);
x = (1:1:4);
y1 = psnr_val(:,1);
y2 = psnr_val(:,2);
plot(x, y1, x, y2);
legend({'three steps', 'full'},'Location','southeast');
title('PSNR');
set(gca,'xtick',[1 2 3 4]);
set(gca,'xticklabel',{'8, 8x8','16, 8x8','16, 16x16','16, 16x16'});

subplot(2,1,2)
y1 = sad_val(:,1);
y2 = sad_val(:,2);
plot(x, y1, x, y2);
legend({'three steps', 'full'},'Location','southeast');
title('total SAD');
set(gca,'xtick',[1 2 3 4]);
set(gca,'xticklabel',{'8, 8x8','16, 8x8','16, 16x16','16, 16x16'});

%% Q2
ref = double(imread('data\frame432.jpg'));
tar = double(imread('data\frame439.jpg'));

[motion_vect, sad_v] = full_search(tar,ref,8,8);
img_pred = pred_img(ref, motion_vect, 8);
residual = res_img(img_pred, tar);
pred_f = uint8(img_pred);
res_f = uint8(residual);
psnr_v = cal_PSNR(uint8(tar), pred_f);

imwrite(pred_f,'predict\Q2.jpg');
imwrite(res_f,'residual\Q2_res.jpg');

figure(10)
x = (1:1:4);
y = [psnr_val(1,2),psnr_val(2,2),psnr_val(3,2),psnr_v];
plot(x, y);
title('PSNR');
set(gca,'xtick',[1 2 3 4]);
set(gca,'xticklabel',{'Q1 8, 8x8','Q1 16, 8x8','Q1 8, 16x16','Q2 8, 8x8'});

%% Q3
fprintf('p = 8 : three steps predict time = %f sec\n', t1(1,1));
fprintf('p = 8 : full search predict time = %f sec\n', t2(1,1));
fprintf('p = 16 : three steps predict time = %f sec\n', t1(2,1));
fprintf('p = 16 : full search predict time = %f sec\n', t2(2,1));