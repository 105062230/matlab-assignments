function [motion_vect, sad_sum] = three_steps(tar, ref, s, p)

[row ,col, ch] = size(ref); 
vectors = zeros(row,col,2);
sads = ones(3, 3) * 65537;
stepMax = p/2;
sad_sum = 0;

count = 1;
for i = 1 : s : row-s+1
    for j = 1 : s : col-s+1
        curblk = tar(i:i+s-1,j:j+s-1,:);
        x = j;
        y = i;
        sads(2,2) = SAD(curblk, ref(i:i+s-1,j:j+s-1,:), s);
        stepSize = stepMax;               
        while(stepSize >= 1)  
            for m = -stepSize : stepSize : stepSize        
                for n = -stepSize : stepSize : stepSize
                    refBlkVer = y + m;
                    refBlkHor = x + n;
                    if ( refBlkVer < 1 || refBlkVer+s-1 > row || refBlkHor < 1 || refBlkHor+s-1 > col)
                        continue;
                    end
                    costRow = m/stepSize + 2;
                    costCol = n/stepSize + 2;
                    if (costRow == 2 && costCol == 2)
                        continue
                    end
                    sads(costRow, costCol) = SAD(curblk,ref(refBlkVer:refBlkVer+s-1, refBlkHor:refBlkHor+s-1,:), s);
                end
            end
        
            [c_r, c_c] = size(sads);
            min = 65537;
            for ci = 1:c_r
                for cj = 1:c_c
                    if (sads(ci,cj) < min)
                        min = sads(ci,cj);
                        dx = cj; dy = ci;
                    end
                end
            end
            
            x = x + (dx-2)*stepSize;
            y = y + (dy-2)*stepSize;
            stepSize = stepSize / 2;
            sads = ones(3, 3) * 65537;
            sads(2,2) = sads(dy,dx);
        end
        sad_sum = sad_sum + min;
        vectors(i,j,1) = y;
        vectors(i,j,2) = x;
        count = count + 1;
        sads = ones(3,3) * 65537;
    end
end
motion_vect = vectors;

