function psnr = cal_PSNR(tar, comp)
[r, c, ch] = size(tar);
MSE = sum(sum(sum((tar-comp).^2)))/(r*c*3);
psnr = 10*log10(255^2/MSE);
end
