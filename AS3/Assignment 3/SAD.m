function sad = SAD(curblk,refblk, n)
sad = 0;
for i = 1:n
    for j = 1:n
        for k = 1:3
            sad = sad + abs((curblk(i,j,k) - refblk(i,j,k)));
        end
    end
end
end