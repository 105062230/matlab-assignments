# Matlab assignments
## 多媒體技術概論assignments ##

### AS1 image compression, dithering and covolution ###
- Q1 DCT image compression
- Q2 Convert the image with different methods of dithering
- Q3 Implement the image convolution by Gaussian filter for image smoothing

### AS2 audio signal ###
- Q1 Create own FIR filters to filter audio signal
- Q2 Audio dithering and noise shaping

### AS3 video motion estimation ###
- Implement two search algorithms to find motion vectors, the Full Search and the 3-Step Search method

### AS4 Bézier curve and 3D Models ###