%%% HW2_Q1.m - Complete the procedure of separating HW2_mix.wav into 3 songs

%% Clean variables and screen
close all;
clear;
clc;

%% Visualization parameters (Change it if you want)
% Some Tips:
% (Tip 1) You can change the axis range to make the plotted result more clearly 
% (Tip 2) You can use subplot function to show multiple spectrums / shapes in one figure
titlefont = 15;
fontsize = 13;
LineWidth = 1.5;

%% 1. Read in input audio file ( audioread )
% y_input: input signal, fs: sampling rate
[y_input, fs] = audioread('HW2_Mix.wav');

%%% Plot example : plot the input audio
% The provided function "make_spectrum" generates frequency
% and magnitude. Use the following example to plot the spectrum.
figure(1);
[frequency, magnitude] = makeSpectrum(y_input, fs);
plot(frequency, magnitude, 'LineWidth', LineWidth); 
title('Spectrum of input signal', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 1600]);

%% 2. Filtering 
% (Hint) Implement my_filter here
fltr_size = 555;
[Signal_low, fltr_low] = myFilter(y_input, fs, fltr_size, 'Blackman', 'low-pass', 280);
[Signal_band, fltr_band] = myFilter(y_input, fs, fltr_size, 'Blackman', 'bandpass', [500, 650]);
[Signal_high, fltr_high] = myFilter(y_input, fs, fltr_size, 'Blackman', 'high-pass', 885);

%%% Plot the shape of filters in Time domain

figure(2);

x = 1:fltr_size;
y_low = fltr_low(x);
y_high = fltr_high(x);
y_band = fltr_band(x);

subplot(3,1,1);
plot(x, y_low);
title('Shape of low', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 fltr_size-1]);

subplot(3,1,2);
plot(x, y_band);
title('Shape of band', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 fltr_size-1]);

subplot(3,1,3);
plot(x, y_high);
title('Shape of high', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 fltr_size-1]);

%%% Plot the spectrum of filters (Frequency Analysis)

figure(3);

[frequency_low, magnitude_low] = makeSpectrum(fltr_low, fs);
[frequency_band, magnitude_band] = makeSpectrum(fltr_band, fs);
[frequency_high, magnitude_high] = makeSpectrum(fltr_high, fs);

subplot(3,1,1);
plot(frequency_low, magnitude_low, 'LineWidth', LineWidth); 
title('Spectrum of low', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 1500]);

subplot(3,1,2);
plot(frequency_band, magnitude_band, 'LineWidth', LineWidth); 
title('Spectrum of band', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 1500]);

subplot(3,1,3);
plot(frequency_high, magnitude_high, 'LineWidth', LineWidth); 
title('Spectrum of high', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 1500]);
%% 3. Save the filtered audio (audiowrite)
% Name the file 'FilterName_para1_para2.wav'
% para means the cutoff frequency that you set for the filter

% audiowrite('FilterName_para1_para2.wav', output_signal1, fs);
audiowrite('output/low-pass_280.wav', Signal_low, fs);
audiowrite('output/bandpass_500_650.wav', Signal_band, fs);
audiowrite('output/high-pass_885.wav', Signal_high, fs);
%%% Plot the spectrum of filtered signals
figure(4);
subplot(3, 1, 1);
[frequency_low, magnitude_low] = makeSpectrum(Signal_low, fs);
plot(frequency_low, magnitude_low, 'LineWidth', LineWidth);
title('Signal of low', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 2000]);

subplot(3, 1, 2);
[frequency_band, magnitude_band] = makeSpectrum(Signal_band, fs);
plot(frequency_band, magnitude_band, 'LineWidth', LineWidth); 
title('Signal of band', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 2000]);

subplot(3, 1, 3);
[frequency_high, magnitude_high] = makeSpectrum(Signal_high, fs);
plot(frequency_high, magnitude_high, 'LineWidth', LineWidth);
title('Signal of high', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 2000]);
%% 4, Reduce the sample rate of the three separated songs to 2kHz.

resmple_low = resample(Signal_low, 2000,44100);
resmple_band = resample(Signal_band, 2000,44100);
resmple_high = resample(Signal_high, 2000,44100);

%% 4. Save the files after changing the sampling rate

audiowrite('output/low-pass_280_2kHz.wav', resmple_low, 2000);
audiowrite('output/bandpass_500_650_2kHz.wav', resmple_band, 2000);
audiowrite('output/high-pass_885_2kHz.wav', resmple_high, 2000);

%% 5. one-fold echo and multiple-fold echo (slide #69)
one = Signal_low;
multiple = Signal_low;

for i = 3201 : length(Signal_low)
    one(i) = Signal_low(i) + 0.8*Signal_low(i-3200); 
    multiple(i) = Signal_low(i) + 0.8*multiple(i-3200); 
end
one = normalize(one, 'range', [-1 1]);
multiple = normalize(multiple, 'range', [-1 1]);
%% 5. Save the echo audios  'Echo_one.wav' and 'Echo_multiple.wav'
audiowrite('output/Echo_one.wav', one, fs);
audiowrite('output/Echo_multiple.wav', multiple, fs);
