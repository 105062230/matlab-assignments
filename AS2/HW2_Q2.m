%%% HW2_Q2.m - bit reduction -> audio dithering -> noise shaping -> low-pass filter -> audio limiting -> normalization
%%% Follow the instructions (hints) and you can finish the homework

%% Clean variables and screen
clear all;close all;clc;

%% Visualization parameters (Change it if you want)
% Some Tips:
% (Tip 1) You can change the axis range to make the plotted result more clearly 
% (Tip 2) You can use subplot function to show multiple spectrums / shapes in one figure
titlefont = 15;
fontsize = 13;
LineWidth = 1.5;

%% 1. Read in input audio file ( audioread )
[input, fs] = audioread('Tempest.wav');


%%% Plot the spectrum of input audio
figure(1);
subplot(3, 1, 1);
[frequency, magnitude] = makeSpectrum(input, fs);
plot(frequency, magnitude, 'LineWidth', LineWidth); 
title('Spectrum of input signal', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 3500]);
%%% Plot the shape of input audio

subplot(3, 1, 2);
x = 1:length(input);
y = input(x, 1);
plot(x, y, 'LineWidth', LineWidth); 
title('Shape of input signal 1 ', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)

subplot(3, 1, 3);
y = input(x, 2);
plot(x, y, 'LineWidth', LineWidth); 
title('Shape of input signal 2', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)

%% 2. Bit reduction
% (Hint) The input audio signal is double (-1 ~ 1)
reduced = (floor(input*128)+0.5)/128;

%%% Save audio (audiowrite) Tempest_8bit.wav
% (Hint) remember to save the file with bit = 8
audiowrite('output/Tempest_8bit.wav', reduced, fs, 'BitsPerSample', 8);

%% 3. Audio dithering
% (Hint) add random noise
noise = rand(size(reduced));
noise = normalize(noise, 'range', [-0.1 0.1]);
dithered = reduced + noise;
%%% Plot the spectrum of the dithered result

figure(2);
subplot(3, 1, 1);
[frequency, magnitude] = makeSpectrum(dithered, fs);
plot(frequency, magnitude, 'LineWidth', LineWidth); 
title('Spectrum of dithered', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize);
xlim([0 3500]);

subplot(3, 1, 2);
x = 1:length(dithered);
y = dithered(x, 1);
plot(x, y, 'LineWidth', LineWidth); 
title('Shape of dithered 1', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)

subplot(3, 1, 3);
y = dithered(x, 2);
plot(x, y, 'LineWidth', LineWidth); 
title('Shape of dithered 2', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)

%% 4. First-order feedback loop for Noise shaping
% (Hint) Check the signal value. How do I quantize the dithered signal? maybe scale up first?
dithered = (floor(dithered*128)+0.5)/128;
shaped = zeros(length(dithered), 1);
for i = 1:length(dithered)
    if i == 1
        e1 = 0;
        e2 = 0;
    else
        e1 = reduced(i-1, 1) - shaped(i-1, 1);
        e2 = reduced(i-1, 2) - shaped(i-1, 2);
    end
    shaped(i,1) = dithered(i, 1) + 0.95*e1;
    shaped(i,2) = dithered(i, 2) + 0.95*e2;
end

shaped = (floor(shaped*128)+0.5)/128;

%input_shaped = normalize(input_shaped, 'range', [-1 1]);

%%% Plot the spectrum of noise shaping
figure(3)
subplot(3, 1, 1);
[frequency, magnitude] = makeSpectrum(shaped, fs);
plot(frequency, magnitude, 'LineWidth', LineWidth); 
title('Spectrum of shaped', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)

subplot(3, 1, 2);
x = 1:length(shaped);
y = shaped(x, 1);
plot(x, y, 'LineWidth', LineWidth); 
title('Shape of noise shaped signal 1', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)

subplot(3, 1, 3);
y = shaped(x, 2);
plot(x, y, 'LineWidth', LineWidth); 
title('Shape of noise shaped signal 2', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)

%% 5. Implement Low-pass filter

[Signal_low(:,1), fltr_low1] = myFilter(shaped(:,1), fs, 555, 'Blackman', 'low-pass', 1700);
[Signal_low(:,2), fltr_low2] = myFilter(shaped(:,2), fs, 555, 'Blackman', 'low-pass', 1700);

%% 6. Audio limiting 

Signal_low(Signal_low>0.8)=0.8;
Signal_low(Signal_low<-0.8)=-0.8;

%% 7. Normalization

Signal_low = normalize(Signal_low, 'range', [-1 1]);

%% 6. Save audio (audiowrite) Tempest_Recover.wav

audiowrite('output/Tempest_Recover.wav', Signal_low, fs, 'BitsPerSample', 8);

%%% Plot the spectrum of output audio
[frequency, magnitude] = makeSpectrum(Signal_low, fs);
figure(4);
subplot(2, 1, 1);
plot(frequency, magnitude, 'LineWidth', LineWidth); 
title('Spectrum of recoverd signal', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 3500]);

[frequency, magnitude] = makeSpectrum(input, fs);
subplot(2, 1, 2);
plot(frequency, magnitude, 'LineWidth', LineWidth); 
title('Spectrum of input signal', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
xlim([0 3500]);
%%% Plot the shape of output audio
figure(5);
subplot(2, 1, 1);
x = 1:555;
y1 = Signal_low(x,1);
y2 = Signal_low(x,2);
plot(x, y1, x, y2, 'LineWidth', LineWidth); 
title('Shape of output signal', 'fontsize', titlefont);
set(gca, 'fontsize', fontsize)
legend('signal 1', 'signal 2');