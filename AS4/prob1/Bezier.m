function [x,y] = Bezier(n,t,p)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
xsum = 0;
ysum = 0;
for k = 0 : n
    B = nchoosek(n,k)* t^k * (1-t)^(n-k);
    xsum = p(k,1) * B; 
    ysum = p(k,2) * B;
end
x = xsum;
y = ysum;
end

