clear all; close all; clc

img = im2double(imread('./bg.png'));
[h, w, ~] = size(img);
% imshow(img);

points = importdata('./points.txt');

%% Curve Computation
n = size(points, 1); % number of input points

%==============================================================%
% Code here. Store the results as `result1`, `result2`
%==============================================================%
result1 = [];
result2 = [];
for i = 1:3:n-3
    for t = 0:0.2:1
        result1 = [result1; [(1-t)^3 3*t*(1-t)^2 3*t^2*(1-t) t^3] * [points(i,:) ; points(i+1,:) ; points(i+2,:) ; points(i+3,:)] ];
    end
    for t = 0:0.01:1
        result2 = [result2; [(1-t)^3 3*t*(1-t)^2 3*t^2*(1-t) t^3] * [points(i,:) ; points(i+1,:) ; points(i+2,:) ; points(i+3,:)] ];
    end
end

% Draw the polygon of the curve
f = figure('position', [0 0 900 900]);
subplot(2, 1, 1);
imshow(img);
hold on
plot(points(:, 1), points(:, 2), 'r.');
plot(result1(:, 1), result1(:, 2), 'g-');
title('low');

subplot(2, 1, 2);
imshow(img);
hold on
plot(points(:, 1), points(:, 2), 'r.');
plot(result2(:, 1), result2(:, 2), 'g-');
title('high');
saveas(f, '1a.png');
%% Scaling
points = points .* 4;
img2 = imresize(img, 4, 'nearest');
%==============================================================%
% Code here.
%==============================================================%
result3 = [];
for i = 1:3:n-3
    for t = 0:0.01:1
        result3 = [result3; [(1-t)^3 3*t*(1-t)^2 3*t^2*(1-t) t^3] * [points(i,:) ; points(i+1,:) ; points(i+2,:) ; points(i+3,:)] ];
    end
end

% Draw the polygon of the curve
f = figure;
imshow(img2);
hold on
plot(points(:, 1), points(:, 2), 'r.');
plot(result3(:, 1), result3(:, 2), 'g-');
saveas(f, '1b.png');