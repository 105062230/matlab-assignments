clear all; close all; clc; 

%% Read OBJ file
obj = readObj('trump.obj');
tval = display_obj(obj,'tumpLPcolors.png');

%==============================================================%
% Code here. Move object center to (0, 0)
% Center is defined as [(max(x) + min(x)) / 2, (max(y) + min(y)) / 2, (max(z) + min(z)) / 2]
% where x, y, z are the vertices of the object.
center = [(max(obj.v(:,1)) + min(obj.v(:,1))) / 2, (max(obj.v(:,2)) + min(obj.v(:,2))) / 2, (max(obj.v(:,3)) + min(obj.v(:,3))) / 2];
[n1,n2] = size(obj.v);
for i = 1:n1
    obj.v(i,1) = obj.v(i,1) - center(1);
    obj.v(i,2) = obj.v(i,2) - center(2);
    obj.v(i,3) = obj.v(i,3) - center(3);
end
%==============================================================%

%% a.
f = figure; 
trisurf(obj.f.v, obj.v(:,1), obj.v(:,2), obj.v(:,3), ...
    'FaceVertexCData', tval, 'FaceColor', 'interp', 'EdgeAlpha', 0);
xlabel('X'); ylabel('Y'); zlabel('Z');
axis equal;
saveas(f, '2a.png');

%==============================================================%
% Code here. Other Problems
%==============================================================%
%% b
% Based on (a), generate a HSV color cone (radius = 1, height = 1) on the x-y
% plane. Peak is at (0, 0, -1.4) and is pointing -z. Show both objects (trump &
% cone) at the same time and save the 3D figure as 2b.png

H = repmat(linspace(0, 1, 100), 100, 1); 
S = repmat([linspace(0, 1, 50) ...
            linspace(1, 0, 50)].', 1, 100);  
V = repmat([ones(1, 50) ...                 
            linspace(1, 0, 50)].', 1, 100);
hsvImage = cat(3, H, S, V);
C = hsv2rgb(hsvImage);

theta = linspace(0, 2*pi, 100);
X = [zeros(1, 100);cos(theta);zeros(1, 100)];
Y = [zeros(1, 100);sin(theta);zeros(1, 100)];
Z = [-0.4.*ones(2, 100);-1.4.*ones(1, 100)];

f2 = figure;
trisurf(obj.f.v, obj.v(:,1), obj.v(:,2), obj.v(:,3), ...
    'FaceVertexCData', tval, 'FaceColor', 'interp', 'EdgeAlpha', 0);
xlabel('X'); ylabel('Y'); zlabel('Z');
axis equal;
hold on;
surf(X, Y, Z, C, 'FaceColor', 'texturemap', 'EdgeColor', 'none');
saveas(f2, '2b.png');

%% c
% Based on (b), try 2 lightning: 1 positional light and 1 directional light. Other
% lighting parameters is up to you. Results are saved as 2c.png with 2 subplots.
f3 = figure;
subplot(1,2,1);
trisurf(obj.f.v, obj.v(:,1), obj.v(:,2), obj.v(:,3), ...
    'FaceVertexCData', tval, 'FaceColor', 'interp', 'EdgeAlpha', 0);
xlabel('X'); ylabel('Y'); zlabel('Z');
axis equal;
hold on;
surf(X, Y, Z, C, 'FaceColor', 'texturemap', 'EdgeColor', 'none');
light('Position',[1 0 0],'Style','local')
title('Positional');

subplot(1,2,2);
trisurf(obj.f.v, obj.v(:,1), obj.v(:,2), obj.v(:,3), ...
    'FaceVertexCData', tval, 'FaceColor', 'interp', 'EdgeAlpha', 0);
xlabel('X'); ylabel('Y'); zlabel('Z');
axis equal;
hold on;
surf(X, Y, Z, C, 'FaceColor', 'texturemap', 'EdgeColor', 'none');
light('Position',[1 0 0], 'Style', 'infinite');
title('Directional');

saveas(f3, '2c.png');
%% d
% Based on (b), use the light light('Position',[0 0 1], 'Style',
% 'infinite'); Try different ambient strength ka, diffuse strength kd, specular
% strength ks.
f4 = figure;
subplot(2, 2, 1);
trisurf(obj.f.v, obj.v(:,1), obj.v(:,2), obj.v(:,3), ...
    'FaceVertexCData', tval, 'FaceColor', 'interp', 'EdgeAlpha', 0, ...
    'AmbientStrength', 1.0, 'DiffuseStrength', 0.0, 'SpecularStrength', 0.0);
xlabel('X'); ylabel('Y'); zlabel('Z');
axis equal;
hold on;
surf(X, Y, Z, C, 'FaceColor', 'texturemap', 'EdgeColor', 'none', ...
    'AmbientStrength', 1.0, 'DiffuseStrength', 0.0, 'SpecularStrength', 0.0);
light('position', [0 0 1], 'Style', 'infinite');
title('I. (ka, kd, ks) = (1.0, 0.0, 0.0)');

subplot(2, 2, 2);
trisurf(obj.f.v, obj.v(:,1), obj.v(:,2), obj.v(:,3), ...
    'FaceVertexCData', tval, 'FaceColor', 'interp', 'EdgeAlpha', 0, ...
    'AmbientStrength', 0.1, 'DiffuseStrength', 1.0, 'SpecularStrength', 0.0);
xlabel('X'); ylabel('Y'); zlabel('Z');
axis equal;
hold on;
surf(X, Y, Z, C, 'FaceColor', 'texturemap', 'EdgeColor', 'none', ...
    'AmbientStrength', 0.1, 'DiffuseStrength', 1.0, 'SpecularStrength', 0.0);
light('position', [0 0 1], 'Style', 'infinite');
title('II. (ka, kd, ks) = (0.1, 1.0, 0.0)');

subplot(2, 2, 3);
trisurf(obj.f.v, obj.v(:,1), obj.v(:,2), obj.v(:,3), ...
    'FaceVertexCData', tval, 'FaceColor', 'interp', 'EdgeAlpha', 0, ...
    'AmbientStrength', 0.1, 'DiffuseStrength', 0.1, 'SpecularStrength', 1.0);
xlabel('X'); ylabel('Y'); zlabel('Z');
axis equal;
hold on;
surf(X, Y, Z, C, 'FaceColor', 'texturemap', 'EdgeColor', 'none', ...
    'AmbientStrength', 0.1, 'DiffuseStrength', 0.1, 'SpecularStrength', 1.0);
light('position', [0 0 1], 'Style', 'infinite');
title('III. (ka, kd, ks) = (0.1, 0.1, 1.0)');

subplot(2, 2, 4);
trisurf(obj.f.v, obj.v(:,1), obj.v(:,2), obj.v(:,3), ...
    'FaceVertexCData', tval, 'FaceColor', 'interp', 'EdgeAlpha', 0, ...
    'AmbientStrength', 0.1, 'DiffuseStrength', 0.5, 'SpecularStrength', 1.0);
xlabel('X'); ylabel('Y'); zlabel('Z');
axis equal;
hold on;
surf(X, Y, Z, C, 'FaceColor', 'texturemap', 'EdgeColor', 'none', ...
    'AmbientStrength', 0.1, 'DiffuseStrength', 0.5, 'SpecularStrength', 1.0);
light('position', [0 0 1], 'Style', 'infinite');
title('IV. (ka, kd, ks) = (0.1, 0.5, 1.0)');

saveas(f4, '2d.png');